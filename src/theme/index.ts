import { extendTheme } from "@chakra-ui/react";

const theme = extendTheme({
  styles: {
    global: {
      "#root": {
        width: "100%",
      },
    },
  },
  colors: {
    primary: {
      dark: "#5038ED",
      dark1: "#6450e6",
      light: "#9181F4",
    },
  },
});

export default theme;
