import React from "react";
import { InputGroup, InputLeftElement, Input } from "@chakra-ui/input";

const SimpleInput: React.FC<SimpleInputProps> = ({
  placeholder,
  icon,
  ...args
}) => {
  return (
    <InputGroup>
      <InputLeftElement pointerEvents="none">{icon}</InputLeftElement>
      <Input
        type="tel"
        placeholder={placeholder}
        {...args}
        fontSize={{
          base: "sm",
          xl: "md",
        }}
        border={"1px solid"}
        borderColor={"gray.400"}
      />
    </InputGroup>
  );
};

export default SimpleInput;

interface SimpleInputProps {
  placeholder?: string;
  icon?: React.ReactNode;
}
