import React from "react";
import { Button, Spinner } from "@chakra-ui/react";

const PrimaryButton: React.FC<PrimaryButtonProps> = ({
  label,
  type = "button",
  isLoading = false,
}) => {
  return (
    <Button
      width={"full"}
      background={"primary.dark"}
      color={"white"}
      fontWeight={"medium"}
      letterSpacing={0.5}
      fontSize={14}
      _hover={
        !isLoading
          ? {
              background: "primary.dark1",
            }
          : {}
      }
      _focus={{}}
      _active={{}}
      type={type}
    >
      {isLoading ? <Spinner /> : label}
    </Button>
  );
};

export default PrimaryButton;

interface PrimaryButtonProps {
  label: string;
  type?: "button" | "submit" | "reset";
  isLoading?: boolean;
}
