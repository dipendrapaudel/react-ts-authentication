import { Link as ReactRouterLink } from "react-router-dom";
import { Link as ChakraLink } from "@chakra-ui/react";

const Link: React.FC<LinkProps> = ({ href, label }) => {
  return (
    <ChakraLink
      as={ReactRouterLink}
      to={href}
      fontWeight={"normal"}
      textDecoration={"underline"}
    >
      {label}
    </ChakraLink>
  );
};

export default Link;

interface LinkProps {
  href: string;
  label: string;
}
