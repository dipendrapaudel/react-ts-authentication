import { useState } from "react";
import { Box, Heading, Text, VStack } from "@chakra-ui/react";
import { FiUser } from "react-icons/fi";
import { BiSolidLock } from "react-icons/bi";
import SimpleInput from "../../components/Form/SimpleInput";
import PrimaryButton from "../../components/Button/PrimaryButton";
import Link from "../../components/Link";

const inputFields = [
  {
    name: "username",
    placeholder: "Enter username",
    icon: <FiUser size={20} />,
    type: "text",
  },
  {
    name: "password",
    placeholder: "Enter password",
    icon: <BiSolidLock size={20} />,
    type: "password",
  },
  {
    name: "confirm_password",
    placeholder: "Re-enter password",
    icon: <BiSolidLock size={20} />,
    type: "password",
  },
];

const RegisterForm = () => {
  const [loading, setLoading] = useState(false);

  const handleRegister = (event: React.FormEvent) => {
    event.preventDefault();

    if (loading) return;

    setLoading(true);

    setTimeout(() => setLoading(false), 3000);
  };

  return (
    <Box width={"360px"} maxW={"80%"} textAlign={"center"}>
      <Heading fontSize={40}>Register</Heading>
      <Text color={"gray.600"} mt={2}>
        Please fill in the required information below:
      </Text>

      <form onSubmit={handleRegister}>
        <VStack py={5} gap={"18px"} alignItems={"stretch"}>
          {inputFields.map((inputField) => {
            return <SimpleInput key={inputField.name} {...inputField} />;
          })}

          <PrimaryButton label="Register" type={"submit"} isLoading={loading} />
        </VStack>
      </form>

      <Text>
        Already have an account? <Link href="/login" label="Login Now" />
      </Text>
    </Box>
  );
};

export default RegisterForm;
