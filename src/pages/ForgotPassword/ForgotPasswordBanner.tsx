import { Box, Image, Text } from "@chakra-ui/react";

const LoginBanner = () => {
  return (
    <Box
      width={"412px"}
      height={"524px"}
      maxW={"80%"}
      background={"primary.light"}
      rounded={"46px"}
      position={"relative"}
      py={"26px"}
      px={"30px"}
    >
      <Text fontSize={32} fontWeight={"bold"} color={"white"} width={"50%"}>
        Very good works are waiting for you !!!
      </Text>

      <Image
        src={"/images/women with tab.png"}
        alt="Women With Tab"
        position={"absolute"}
        bottom={0}
        right={-30}
      />

      <Image
        src={"/images/instant.png"}
        position={"absolute"}
        bottom={98}
        left={-30}
      />
    </Box>
  );
};

export default LoginBanner;
