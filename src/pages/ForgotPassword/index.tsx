import { HStack, StackItem } from "@chakra-ui/react";
import ForgotPasswordForm from "./ForgotPasswordForm";
import ForgotPasswordBanner from "./ForgotPasswordBanner";

const Login = () => {
  return (
    <HStack minH={"100vh"} width={"100%"} alignItems={"stretch"} gap={0}>
      <StackItem flex={1} display={"grid"} placeItems={"center"}>
        <ForgotPasswordForm />
      </StackItem>

      <StackItem
        flex={1}
        background={"primary.light"}
        backgroundImage={"url('/images/background.svg')"}
        backgroundSize="cover"
        backgroundRepeat={"no-repeat"}
        display={{
          base: "none",
          lg: "grid",
        }}
        placeItems={"center"}
      >
        <ForgotPasswordBanner />
      </StackItem>
    </HStack>
  );
};

export default Login;
