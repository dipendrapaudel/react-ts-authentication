import { useState } from "react";
import { Box, Heading, Text, VStack } from "@chakra-ui/react";
import { FiUser } from "react-icons/fi";
import SimpleInput from "../../components/Form/SimpleInput";
import PrimaryButton from "../../components/Button/PrimaryButton";
import Link from "../../components/Link";

const inputFields = [
  {
    name: "email",
    placeholder: "Enter your email",
    icon: <FiUser size={20} />,
  },
];

const LoginForm = () => {
  const [loading, setLoading] = useState(false);

  const handleLogin = (event: React.FormEvent) => {
    event.preventDefault();

    if (loading) return;

    setLoading(true);

    setTimeout(() => setLoading(false), 3000);
  };

  return (
    <Box width={"360px"} maxW={"80%"} textAlign={"center"}>
      <Heading fontSize={32}>FORGOT PASSWORD</Heading>
      <Text color={"gray.600"} mt={2}>
        Enter your email address to reset your password:
      </Text>

      <form onSubmit={handleLogin}>
        <VStack py={5} gap={"18px"} alignItems={"stretch"}>
          {inputFields.map((inputField) => {
            return <SimpleInput key={inputField.name} {...inputField} />;
          })}

          <PrimaryButton label="Send OTP" type={"submit"} isLoading={loading} />
        </VStack>
      </form>

      <Text>
        Go back to <Link href="/login" label="Login" />
      </Text>
    </Box>
  );
};

export default LoginForm;
