import { useState } from "react";
import { Box, Heading, Text, VStack, Flex } from "@chakra-ui/react";
import { FiUser } from "react-icons/fi";
import { BiSolidLock } from "react-icons/bi";
import SimpleInput from "../../components/Form/SimpleInput";
import PrimaryButton from "../../components/Button/PrimaryButton";
import Link from "../../components/Link";

const inputFields = [
  {
    name: "username",
    placeholder: "Enter your username",
    icon: <FiUser size={20} />,
  },
  {
    name: "username",
    placeholder: "Enter your password",
    icon: <BiSolidLock size={20} />,
  },
];

const LoginForm = () => {
  const [loading, setLoading] = useState(false);

  const handleLogin = (event: React.FormEvent) => {
    event.preventDefault();

    if (loading) return;

    setLoading(true);

    setTimeout(() => setLoading(false), 3000);
  };

  return (
    <Box width={"360px"} maxW={"80%"} textAlign={"center"}>
      <Heading fontSize={40}>LOGIN</Heading>
      <Text color={"gray.600"} mt={2}>
        Please enter your credentials to log in.
      </Text>

      <form onSubmit={handleLogin}>
        <VStack py={5} gap={"18px"} alignItems={"stretch"}>
          {inputFields.map((inputField) => {
            return <SimpleInput key={inputField.name} {...inputField} />;
          })}

          <Flex justifyContent={"flex-end"}>
            <Link href="/forgot-password" label="Forgot Password?" />
          </Flex>

          <PrimaryButton label="Login" type={"submit"} isLoading={loading} />
        </VStack>
      </form>

      <Text>
        Don't have an account? <Link href="/register" label="Register Now" />
      </Text>
    </Box>
  );
};

export default LoginForm;
