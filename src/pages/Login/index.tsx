import { HStack, StackItem } from "@chakra-ui/react";
import LoginForm from "./LoginForm";
import LoginBanner from "./LoginBanner";

const Login = () => {
  return (
    <HStack minH={"100vh"} width={"100%"} alignItems={"stretch"} gap={0}>
      <StackItem flex={1} display={"grid"} placeItems={"center"}>
        <LoginForm />
      </StackItem>

      <StackItem
        flex={1}
        background={"primary.light"}
        backgroundImage={"url('/images/background.svg')"}
        backgroundSize="cover"
        backgroundRepeat={"no-repeat"}
        display={{
          base: "none",
          lg: "grid",
        }}
        placeItems={"center"}
      >
        <LoginBanner />
      </StackItem>
    </HStack>
  );
};

export default Login;
